      // tslint:disable: no-unused-expression
import { expect } from "chai";

import { BioSeq } from '../src/BioSeq'
import { BioSeqSet } from '../src/BioSeqSet'

describe('BioSeqSet', () => {
  describe('constructor', () => {
    it('should init with initial set', () => {
      const bioseq = new BioSeq('seq11', 'AAAAAAA')
      const bioseqset1 = new BioSeqSet([bioseq])
      expect(bioseqset1.getBioSeqs()).eql([bioseq])
    })
    it('should calculate initial partition', () => {
      const bioseq = new BioSeq('seq11', 'AAAAAAA')
      const bioseqset1 = new BioSeqSet([bioseq])
      const expectedPartitions = [[1, 7]]
      expect(bioseqset1.getPartitions()).eql(expectedPartitions)
    })
    it('should take the partition given with initial set', () => {
      const bioseq = new BioSeq('seq11', 'AAAAAAA')
      const givenPartition = [[1,3], [4, 7]]
      const bioseqset1 = new BioSeqSet([bioseq], [[1,3], [4, 7]])
      expect(bioseqset1.getBioSeqs()).eql([bioseq])
    })
  })
  describe('maxLength', () => {
    it('should give the right result', () => {
      const sequence = 'AAAAAA'
      const longerSequence = sequence + 'AA'
      const expected = 8
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('longerSeq', longerSequence)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      expect(bioseqset.maxLength()).eql(expected)
    })
    it('should give the right result counting gaps', () => {
      const sequence       = 'AA---AAAAAA'
      const longerSequence = 'AA-----AAAAA'
      const expected = 12
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('longerSeq', longerSequence)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      expect(bioseqset.maxLength()).eql(expected)
    })
    it('should give the right result without counting gaps if gapless = true is passed', () => {
      const trueLongerSequence = 'AA---AAAAAA'
      const sequence =           'AA-----AAAAA'
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('trueLongerSeq', trueLongerSequence)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      const expected = 8
      expect(bioseqset.maxLength(true)).eql(expected)
    })
  })
  describe('concatSequences', () => {
    it('should concat sequences from two BioSeqSet', () => {
      const bioseq11 = new BioSeq('seq11', 'AAAAA')
      const bioseq12 = new BioSeq('seq12', 'CCCCCCC')
      const bioseq21 = new BioSeq('seq21', 'DDDDDD')
      const bioseq22 = new BioSeq('seq22', 'EEEE')
      
      const bioseqset1 = new BioSeqSet([bioseq11, bioseq12])
      const bioseqset2 = new BioSeqSet([bioseq21, bioseq22])

      const expected1 = new BioSeq('seq11', 'AAAAA--DDDDDD')
      const expected2 = new BioSeq('seq12', 'CCCCCCCEEEE--')

      const concatExpected = new BioSeqSet([expected1, expected2])

      const concat = bioseqset1.concatSequences(bioseqset2)
      expect(concat.getBioSeqs()).eql(concatExpected.getBioSeqs())
    })
    it('should calculate the correct partitions after concat of two BioSeqSet', () => {
      const bioseq11 = new BioSeq('seq11', 'AAAAA')
      const bioseq12 = new BioSeq('seq12', 'CCCCCCC')
      const bioseq21 = new BioSeq('seq21', 'DDDDDD')
      const bioseq22 = new BioSeq('seq22', 'EEEE')
      
      const bioseqset1 = new BioSeqSet([bioseq11, bioseq12])
      const bioseqset2 = new BioSeqSet([bioseq21, bioseq22])

      const expectedPartition = [
        [1, 7],
        [8, 13]
      ]

      const concat = bioseqset1.concatSequences(bioseqset2)
      expect(concat.getPartitions()).eql(expectedPartition)
    })
  })
  describe('isAligned()', () => {
    it('should be false if sequences are not of the same size', () => {
      const sequence       = 'AA---AAAAAA'
      const longerSequence = 'AA-----AAAAA'
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('longerSeq', longerSequence)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      expect(bioseqset.isAligned()).to.be.false
    })
    it('should be true if sequences are not of the same size', () => {
      const sequence1 = 'AA-C--AAAAAAA--'
      const sequence2 = 'AA-----AAAAA---'
      const bioseq1 = new BioSeq('seq1', sequence1)
      const bioseq2 = new BioSeq('seq2', sequence2)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      expect(bioseqset.isAligned()).to.be.true
    })
  })
  describe('getHash()', () => {
    it('it should return a sha-256 of the dataset', () => {
      const sequence       = 'AA---AAAAAA'
      const longerSequence = 'AA-----AAAAA'
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('longerSeq', longerSequence)
      const bioseqset = new BioSeqSet([bioseq1, bioseq2])
      const hash = bioseqset.getHash()
      expect(hash).eql('9d38cef8943373d5c0dd9a66b7d9132b82fa75c7595a2f4f2f81b815afc960c3')
    })
    it('should have the same hash even if the order is not the same', () => {
      const sequence       = 'AA---AAAAAA'
      const longerSequence = 'AA-----AAAAA'
      const bioseq1 = new BioSeq('seq', sequence)
      const bioseq2 = new BioSeq('longerSeq', longerSequence)
      const bioseqset = new BioSeqSet([bioseq2, bioseq1])
      const hash = bioseqset.getHash()
      expect(hash).eql('9d38cef8943373d5c0dd9a66b7d9132b82fa75c7595a2f4f2f81b815afc960c3')
    })
  })
})
