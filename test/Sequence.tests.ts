import { expect } from 'chai'

import { Sequence } from '../src/Sequence'

describe('Sequence', () => {
  describe('.seq', () => {
    it('should return the sequence', () => {
      const sequence = 'AACC'
      const seqObj = new Sequence(sequence)
      expect(seqObj.seq).eql(sequence)
    })
  })
  describe('noGaps()', () => {
    it('should return the sequence without gaps and not change the original sequence', () => {
      const sequence = 'AACC-DD'
      const seqObj = new Sequence(sequence)
      const expected = 'AACCDD'
      expect(seqObj.noGaps()).eql(expected)
      expect(seqObj.seq).eql(sequence)
    })
    it('should return the sequence without any gaps and not change the original sequence', () => {
      const sequence = 'AA-C-C-D----D'
      const seqObj = new Sequence(sequence)
      const expected = 'AACCDD'
      expect(seqObj.noGaps()).eql(expected)
      expect(seqObj.seq).eql(sequence)
    })
  })
  describe('aseq()', () => {
    it('should return the aseq of the sequence', () => {
      const sequence = 'AACCDD'
      const seqObj = new Sequence(sequence)
      const expected = 'JRe0BgnR26qsW1KZoGHq_g'
      expect(seqObj.aseq()).eql(expected)
    })
    it('should return the same aseq if the sequence has gaps', () => {
      const sequence = 'AAC-CDD'
      const seqObj = new Sequence(sequence)
      const expected = 'JRe0BgnR26qsW1KZoGHq_g'
      expect(seqObj.aseq()).eql(expected)
    })
  })
})