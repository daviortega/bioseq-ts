import { expect } from "chai";
import fs from 'fs'
import through2 from "through2";

import { LogLevelDescType } from "loglevel-colored-prefix";
import { BioSeq } from '../src/BioSeq'
import { BioSeqSet } from "../src/BioSeqSet";
import { FastaParserStream } from "../src/FastaParserStream";

const loglevel: LogLevelDescType = 'info'

describe(`FastaParserStream`, () => {
  it('should parse all sequences', (done) => {
    let counter = 0;
    const expectedCount = 198900
    const readable = fs.createReadStream('datasets/secretins.fa')
    const parser = new FastaParserStream(loglevel);
    readable.pipe(parser)
      .on('data', () => {
        counter++
      })
      .on('end', () => {
        expect(counter).eql(expectedCount)
        done()
      })
  })
})