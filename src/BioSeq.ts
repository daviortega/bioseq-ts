import { Sequence } from './Sequence';

/**
 * Defines a biological sequence as header and a Sequence object
 *
 * @class BioSeq
 * @extends {Sequence}
 */
class BioSeq extends Sequence {
  /**
   * Identifier
   *
   * @type {string}
   * @memberof BioSeq
   */
  public readonly header: string;

  /**
   * Creates an instance of BioSeq.
   * @param {string} header
   * @param {string} sequence
   * @memberof BioSeq
   */
  constructor(header: string, sequence: string) {
    super(sequence);
    this.header = header;
  }
}

export { BioSeq };
