import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

import { BioSeqSet } from './BioSeqSet';
import { ParseUtils } from './ParseUtilsAbstract';

/**
 * Utils for PHYLIP format as defined by [PHYLIP] (http://evolution.genetics.washington.edu/phylip/doc/sequence.html)
 *
 * @class PhylipUtils
 * @extends {ParseUtils}
 */
class PhylipUtils extends ParseUtils {
  protected log: Logger;
  protected logLevel: LogLevelDescType;

  constructor(logLevel: LogLevelDescType = 'INFO') {
    super();
    this.logLevel = logLevel;
    this.log = new Logger(logLevel);
  }

  /**
   * Parse PHYLIP string chunk to a BioSeqSet object
   *
   * @param {string} chunk
   * @returns
   * @memberof PhylipUtils
   */
  public parse(chunk: string): BioSeqSet {
    const log = this.log.getLogger('PhylipUtils::parse');
    log.error('Not implemented');
    throw new Error('Not implemented');
    return new BioSeqSet();
  }

  /**
   * Writes BioSeqSet in PHYLIP format
   *
   * @param {BioSeqSet} input
   * @returns
   * @memberof PhylipUtils
   */
  public write(input: BioSeqSet): string {
    const log = this.log.getLogger('PhylipUtils::write');
    if (!input.isAligned()) {
      log.error('Not an alignment. Sequences must be all the same length (with gaps).');
      throw new Error('Not an alignment. Sequences must be all the same length (with gaps).');
    }
    const bioSeqs = input.getBioSeqs();
    let phylip = `${bioSeqs.length} ${input.maxLength()}\n`;
    bioSeqs.forEach(bioSeq => {
      const header = `${bioSeq.header.slice(0, 9)}${
        bioSeq.header.length < 9 ? ' '.repeat(9 - bioSeq.header.length) : ''
      }`;
      phylip += `${header} ${bioSeq.seq}\n`;
    });
    return phylip;
  }
}

export { PhylipUtils };
