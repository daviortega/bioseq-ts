import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

import { BioSeq } from './BioSeq';
import { BioSeqSet } from './BioSeqSet';
import { ParseUtils } from './ParseUtilsAbstract';

class FastaUtils extends ParseUtils {
  protected log: Logger;
  protected logLevel: LogLevelDescType;

  constructor(logLevel: LogLevelDescType = 'INFO') {
    super();
    this.logLevel = logLevel;
    this.log = new Logger(logLevel);
  }

  /**
   * Parse FASTA string chunk to a BioSeqSet object
   *
   * @param {string} chunk
   * @returns
   * @memberof FastaUtils
   */
  public parse(chunk: string) {
    const log = this.log.getLogger('BioSeq::parse');
    if (chunk[0] !== '>') {
      throw Error(`invalid fasta`);
    }
    const set: BioSeq[] = [];
    const seqs = chunk.split('>').filter(i => i !== '');
    seqs.forEach(seq => {
      const parts = seq.split('\n');
      const header = parts.shift();
      const sequence = parts.join('').replace(/ /g, '');
      if (header && sequence && parts.length > 0) {
        const bioseq = new BioSeq(header, sequence);
        set.push(bioseq);
      } else {
        throw Error(`Invalid FASTA.\nOffending segment: ${seq}`);
      }
    });
    const bioSeqSet = new BioSeqSet(set);
    log.debug(`Parsed ${bioSeqSet.getBioSeqs().length} sequences`);
    return bioSeqSet;
  }

  public write(input: BioSeqSet) {
    const log = this.log.getLogger('BioSeq::write');
    let fasta = '';
    log.debug(`Making Fasta formated string of ${input.getBioSeqs.length} sequences`);
    input.getBioSeqs().forEach(bioseq => {
      fasta += `>${bioseq.header}\n${bioseq.seq}\n`;
    });
    return fasta;
  }
}

export { FastaUtils };
