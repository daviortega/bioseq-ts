import crypto from 'crypto';
import { BioSeq } from './BioSeq';

type IPartition = [number, number];

class BioSeqSet {
  /**
   * Contains the list of BioSeq Objects
   *
   * @private
   * @type {BioSeq[]}
   * @memberof BioSeqSet
   */
  private readonly set: BioSeq[];
  private readonly hash: crypto.Hash;
  private hashString: string;
  /**
   * Contains the partition table of the set
   *
   * @private
   * @type {IPartition[]}
   * @memberof BioSeqSet
   */
  private partitionTable: IPartition[];

  constructor(set: BioSeq[] = [], partitionTable: IPartition[] = []) {
    this.set = set;
    this.partitionTable = this.isValidPartitionTable(partitionTable) ? partitionTable : [this.calcPartitionTable()];
    this.hash = crypto.createHash('sha256');
    this.hashString = '';
  }

  /**
   * Creates a unique identifier based on the content of the dataset
   *
   * @returns {string}
   * @memberof BioSeqSet
   */
  public getHash(): string {
    if (!this.hashString) {
      this.getBioSeqs()
        .map(seq => seq.header + '_' + seq.seq)
        .sort()
        .forEach(item => {
          this.hash.update(item);
        });
      this.hashString = this.hash.digest('hex');
    }
    return this.hashString;
  }

  /**
   * Returns the set of BioSeqs.
   *
   * @returns
   * @memberof BioSeqSet
   */
  public getBioSeqs() {
    return this.set;
  }

  /**
   * Returns the partition of the set
   *
   * @returns
   * @memberof BioSeqSet
   */
  public getPartitions() {
    return this.partitionTable;
  }

  /**
   * Concatenate the sequences of two BioSeqSets. Keep header of the current set.
   *
   * @param {BioSeqSet} newset
   * @memberof BioSeqSet
   */
  public concatSequences(newBioSeqSet: BioSeqSet): BioSeqSet {
    const concatenated: BioSeq[] = [];
    const newset = newBioSeqSet.getBioSeqs();
    if (newset.length !== this.set.length) {
      throw Error('Cannot concat BioSeqSets with different lengths');
    }
    const max1 = this.maxLength();
    const max2 = newBioSeqSet.maxLength();

    this.set.forEach((item, i) => {
      const header = item.header;
      const seq =
        item.seq + '-'.repeat(max1 - item.seq.length) + newset[i].seq + '-'.repeat(max2 - newset[i].seq.length);
      const bioseq = new BioSeq(header, seq);
      concatenated.push(bioseq);
    });
    const newPartitions = this.partitionTable;
    newPartitions.push([max1 + 1, max1 + max2]);
    return new BioSeqSet(concatenated, newPartitions);
  }

  /**
   * Return the length of the longest sequence in the set (with or without gaps).
   *
   * @param {boolean} [gapless=false]
   * @returns {number}
   * @memberof BioSeqSet
   */
  public maxLength(gapless: boolean = false): number {
    let max = 0;
    this.set.forEach(bioseq => {
      let curr = bioseq.seq.length;
      if (gapless) {
        curr = bioseq.noGaps().length;
      }
      max = max > curr ? max : curr;
    });
    return max;
  }

  /**
   * Returns a boolean if all sequences are aligned, or if they all have the same length
   *
   * @returns {boolean}
   * @memberof BioSeqSet
   */
  public isAligned(): boolean {
    const maxL = this.maxLength();
    for (const bioseq of this.set) {
      if (bioseq.seq.length !== maxL) {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns the partition of the dataset. [start, end]
   *
   * @private
   * @returns
   * @memberof BioSeqSet
   */
  private calcPartitionTable(): IPartition {
    const partition: IPartition = [1, this.maxLength()];
    return partition;
  }

  /**
   * Minimal sanity check to see if it is a valid partition table
   *
   * @private
   * @param {IPartition[]} partitionTable
   * @returns {boolean}
   * @memberof BioSeqSet
   */
  private isValidPartitionTable(partitionTable: IPartition[]): boolean {
    let lastCoord = 0;
    for (const partition of partitionTable) {
      const rightStart = partition[0] === lastCoord + 1;
      lastCoord = partition[1];
      const rightOrder = partition[1] - partition[0] > 0;
      if (!rightStart || !rightOrder) {
        return false;
      }
    }
    if (lastCoord !== this.maxLength()) {
      return false;
    }
    return true && partitionTable.length !== 0;
  }
}

export { BioSeqSet };
