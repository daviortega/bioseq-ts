<img src="https://gitlab.com/daviortega/bioseq-ts/raw/master/assets/BioSeqTS.logo.png" width=500px/>

# Development

## Logic

In **BioSeqTS** we keep things separate in different classes.

### `Sequence`

First, biological sequences (DNA, RNA and amino-acids) are transformed in a `Sequence` object. Methods to manipulate the sequence (e.g. reverse complement or remove gaps) should be coded there.

### `BioSeq`

 A `BioSeq` object is formed by a `Sequence` object and a header.


### `BioSeqSet`

The idea here is that a given set of `BioSeqs` makes up for a `BioSeqSet`. Adding sequences after initializing a `BioSeqSet` is not allowed. Our logic is that adding a sequence changes the set and it should be a new set.

What if I need to add sequences?

Simple. Get the BioSeqs, push more sequences to the array and initialize a new set:

```javascript
const bioSeqSet = new BioSeqSet(initialBioSeqs)
const newSeqToBeAdded = new BioSeq('newSeq', 'AAAAAAA')

const bioseqs = bioSeqSet.getBioSeqs()
bioseqs.push(newSeqToBeAdded)

const newSet = new BioSeqSet(biosets)
```

#### Partitions in BioSeqSets

If you are reading here is because you are trying to make some more advance stuff like concatenating alignments.

Granted, it is not that advanced but when you build trees from these alignments, then you will have to pass different partitions so the evolutionary models can be set correctly. To keep all this easy, `BioSeqSets` has a `partitions` property.

You can do much other than read it, but the method `concatSequences` updates it for you. Cool huh?

To read the partitions of a `BioSeqSet` just get it:

```javascript
console.log(bioSeqSet.getPartitions())
/*
[
  [0, 100],
  [101, 200],
  ...
]
*/
```

> Note that one of the reasons to make BioSeqSet immutable is to mantain partitions correct.

... to be continued
